enum json_type {
	 J_STRING,
	 J_NUMBER,
	 J_OBJECT,
	 J_ARRAY,
	 J_NULL,
	 J_BOOL,
	 J_INVALID,
};

struct json_object {
  struct json_value *key;
  struct json_value *value;
  struct json_object *next;
};

typedef struct json_value {
  enum json_type type;
  union {
    char *string;

    int number;

    struct json_object *object;

    struct {
      int len;
      struct json_value **v;
    } array;

    int bool;
  } as;
} json_value;

json_value	*json_parse(char *);

json_value	*json_new_value(enum json_type);
void		 json_del_value(json_value*);

json_value	*read_object(char**);
json_value	*read_array(char**);
json_value	*read_number(char**);
json_value	*read_string(char**);
json_value	*read_true(char**);
json_value	*read_false(char**);
json_value	*read_null(char**);
json_value	*read_value(char**);

void json_dump_value(json_value*);
void json_dump_valueln(json_value*);
