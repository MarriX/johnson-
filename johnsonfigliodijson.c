#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "johnsonfigliodijson.h"

json_value *
json_new_value(enum json_type t)
{
  json_value *v = calloc(1, sizeof(json_value));
  v->type = t;
  return v;
}

void
json_dump_value(json_value *v)
{
  switch (v->type) {
  case J_STRING:
    printf("\"%s\"", v->as.string);
    break;

  case J_NUMBER:
    printf("%d", v->as.number);
    break;

  case J_OBJECT: {
    struct json_object *o = v->as.object;

    printf("{");
    while (o != NULL) {
      json_dump_value(o->key);
      printf(":");
      json_dump_value(o->value);
      printf(",");
      o = o->next;
    }
    printf("}");
    break;
  }

  case J_ARRAY: {
    int i;
    printf("[");
    for (i = 0; i < v->as.array.len; ++i) {
      json_dump_value(v->as.array.v[i]);
      printf(",");
    }
    printf("]");
    break;
  }

  case J_NULL:
    printf("null");
    break;

  case J_BOOL:
    if (v->as.bool)
      printf("true");
    else
      printf("false");
    break;

  case J_INVALID:
    printf("** INVALID **");
    break;
  }
}

void
json_dump_valueln(json_value *v)
{
  json_dump_value(v);
  printf("\n");
}

void
json_del_value(json_value *v)
{
  int i;
  struct json_object *o, *n;

  switch (v->type) {
  case J_STRING:
    free(v->as.string);
    break;

  case J_ARRAY:
    for (i = 0; i < v->as.array.len; ++i)
      json_del_value(v->as.array.v[i]);
    free(v->as.array.v);
    break;

  case J_OBJECT:
    o = v->as.object;
    while (o != NULL) {
      n = o->next;
      json_del_value(o->key);
      json_del_value(o->value);
      free(o);
      o = n;
    }
    break;

  default: break;
  }

  free(v);
}

int read_literal(char *f, char **s){
  while (*f != '\0' && *f++ == *(*s)++)
    ;

  return *f == 0;
}

json_value *
read_object (char **s){
  json_value *o, *k, *v;
  struct json_object *c;

  o = json_new_value(J_OBJECT);
  o->as.object = NULL;

  (*s)++; //salta la { iniziale

  do{
    if(**s == '}'){
      (*s)++;
      return o;
    }

    if(**s != '\"'){
      json_del_value(o);
      return json_new_value(J_INVALID);
    }

    k = read_string(s);
    if(**s != ':'){
      json_del_value(o);
      json_del_value(k);
      return json_new_value(J_INVALID);
    }

    (*s)++;
    v = read_value(s);
    if (v->type == J_INVALID) {
      json_del_value(o);
      json_del_value(k);
      return v;
    }

    c = calloc(1, sizeof(struct json_object));
    c->key = k;
    c->value = v;

    c->next = o->as.object;
    o->as.object = c;

    if(**s == '}'){
      (*s)++;
      return o;
    }

    if (**s != ','){
      json_del_value(o);
      return json_new_value(J_INVALID);
    }
    (*s)++;
  }
  while(1);
}

json_value *
read_array(char **s){
  json_value *v, *i;

  v = json_new_value(J_ARRAY);

  (*s)++; // salta la [ iniziale
  do {
    if (**s == ']') {
      (*s)++;
      return v;
    }

    i = read_value(s);
    if (i->type == J_INVALID) {
      json_del_value(v);
      return i;
    }

    // mettere i in fondo a v->as.array.v
    v->as.array.len += 1;
    v->as.array.v = realloc(v->as.array.v, v->as.array.len * sizeof(json_value*));
    v->as.array.v[v->as.array.len-1] = i;

    if (**s == ']') {
      (*s)++;
      return v;
    }

    if (**s != ',') {
      json_del_value(v);
      return json_new_value(J_INVALID);
    }

    (*s)++;
  } while(1);
}

json_value *
read_number (char **s){
  json_value *v;
  int n = 0;

  while (**s != '\0' && isdigit(**s)) {
    n = n*10 + (**s - '0');
    (*s)++;
  }

  v = json_new_value(J_NUMBER);
  v->as.number = n;
  return v;
}

json_value *
read_string(char **s)
{
  char *start, *end;
  json_value *v;

  // *s == '"'
  (*s)++;
  start = *s;

  while(**s != '\0' && **s !='"')
    (*s)++;

  if (**s == '\0')
    return json_new_value(J_INVALID);

  end = *s;

  v = json_new_value(J_STRING);
  v->as.string = strndup(start, end - start);

  (*s)++;
  return v;
}

json_value *
read_true(char **s)
{
  if (read_literal("true", s)) {
    json_value *v = json_new_value(J_BOOL);
    v->as.bool = 1;
    return v;
  }
  else
    return json_new_value(J_INVALID);
}

json_value *
read_false(char **s)
{
  if (read_literal("false", s)) {
    json_value *v = json_new_value(J_BOOL);
    v->as.bool = 0;
    return v;
  }
  else
    return json_new_value(J_INVALID);
}

json_value *
read_null(char **s)
{
  if (read_literal("null", s))
    return json_new_value(J_NULL);
  else
    return json_new_value(J_INVALID);
}

json_value *
read_value(char **s)
{
  char c = s[0][0];

  switch(c) {
  case '\0':
    return json_new_value(J_INVALID);

  case '"':
    return read_string(s);

  case '{':
    return read_object(s);

  case '[':
    return read_array(s);

  case 'f':
    return read_false(s);

  case 't':
    return read_true(s);

  case 'n':
    return read_null(s);
  }

  if (isdigit(c)){
    return read_number(s);
  }

  return json_new_value(J_INVALID);
}

json_value *
json_parse(char *str)
{
  return read_value(&str);
}
